<?php
/**
 * @file
 * Deal with OAuth connection with remote store functions for the app store client module.
 */

/**
 * Page callback.
 * Get the OAuth access token.
 */
function get_access_token() {
  // Get the parameters for access token.
  $params = _default_params('access_token');
  
  // Generate the URL for access tiken request.
  $url = url(STORE1 . '/oauth/access_token', array('query' => $params));
  $respone = drupal_http_request($url);
  if ($respone->code == 200) {
    // Parse the respone data to an array.
    $token = _parse_token_respone($respone->data);
  
    // Save access_token key & secret to db.
    variable_set('oauth_access_token_key',    $token['oauth_token']);
    variable_set('oauth_access_token_secret', $token['oauth_token_secret']);
  }
  
  return "";
}

/**
 * Util function: Generate default parameters for URL query string.
 */
function _default_params($token_type = 'request_token') {
  // GET consumer key & secret from db.
  $consumer_key      = variable_get('oauth_consumer_key',    NULL);
  $consumer_secret   = variable_get('oauth_consumer_secret', NULL);

  // The default parameters for sending.
  $params = array(
    "oauth_version"           => "1.0",
    "oauth_nonce"             => _generate_nonce(),
    "oauth_timestamp"         => time(),
    "oauth_consumer_key"      => $consumer_key,
    "oauth_signature_method"  => "HMAC-SHA1", 
  );
  
  // If we request for access token will add the oauth_token to the $params array.
  // @TODO: Check to get it some other way.
  if ($token_type == 'access_token') {
    $params['oauth_token'] = $_GET['oauth_token'];
  }
  elseif ($token_type == 'file_request') {
    $params['oauth_token'] = variable_get('oauth_access_token_key', NULL);
  }
  
  // Get the $params with the generated signature.
  $params = _generate_signature($params, $consumer_secret, $token_type);
  
  return $params;
}

/**
 * Util function: Build a signature and add it to the $params array.
 */
function _generate_signature($params, $consumer_secret, $token_type = 'request_token') {
  // Encode $params keys, values, join and then sort.
  $keys   = _urlencode_rfc3986(array_keys($params));
  $values = _urlencode_rfc3986(array_values($params));
  $encode_params = array_combine($keys, $values);
  uksort($encode_params, 'strcmp');
  
  // Convert the $encode_params to string
  $pairs = array();
  foreach ($encode_params as $key => $value) {
    $pairs[] = $key . '=' . $value;   
  }
  $concatenated_params = implode('&', $pairs);
  
  // Form the "Base string".
  $parts = array('GET', STORE1 . '/oauth/' . $token_type, $concatenated_params);
  $parts = _urlencode_rfc3986($parts);
  $base_string = implode('&', $parts);
  
  //$key = _urlencode_rfc3986($consumer_secret) . "&";
  
  // If it's the access token we add the token secret from db.
  // Other way we'll use an empty string.
  // @TODO: Check for another way to pass this token. 
  if ($token_type == 'access_token') {
    $token_secret = variable_get('oauth_token_secret', NULL);
  }
  elseif ($token_type == 'file_request') {
    $token_secret = variable_get('oauth_access_token_secret', NULL);
  }
  else {
    $token_secret = "";
  }
  
  // Generate the key to put in the mixer, for geting the signature.
  $key_parts = array($consumer_secret, $token_secret);
  $key_parts = _urlencode_rfc3986($key_parts);
  $key = implode('&', $key_parts);
  
  // Generate the signature and append it to $params array.
  $params['oauth_signature'] = base64_encode(hash_hmac('sha1', $base_string, $key, TRUE));

  return $params;
}

/**
 * App store configuration form. 
 */
function app_store_client_config_form() {
  $form = array();
  
  $form['store_address'] = array(
    '#type' => 'textfield', 
    '#title' => t('Store address'), 
    '#description' => t('Please fill the store URL address'),
    '#default_value' => variable_get('store_address', NULL), 
    '#size' => 60, 
    '#maxlength' => 128, 
    '#required' => TRUE,
  );
  
  $form['oauth_consumer_key'] = array(
    '#type' => 'textfield', 
    '#title' => t('App store consumer key'), 
    '#description' => t('Please fill (copy & paste) your store consumer key. Ex: d8ZxrMdWnbDGBpnJyQVE5viUMr6xQRga.'),
    '#default_value' => variable_get('oauth_consumer_key', NULL), 
    '#size' => 60, 
    '#maxlength' => 128, 
    '#required' => TRUE,
  );
  
  $form['oauth_consumer_secret'] = array(
    '#type' => 'textfield', 
    '#title' => t('App store consumer secret'), 
    '#description' => t('Please fill (copy & paste) your store consumer secret. Ex: oB6zZw4xLKApowhaDhayMQfc7hAYDDpa.'),
    '#default_value' => variable_get('oauth_consumer_secret', NULL), 
    '#size' => 60, 
    '#maxlength' => 128, 
    '#required' => TRUE,
  );
  
  $form['submit'] = array(
    '#type' => 'submit', 
    '#value' => t('Save')
  );
  
  return $form;
}

/**
 * App store configuration form submit.
 */
function app_store_client_config_form_submit(&$form, &$form_state) {
  // Save store address to db.
  variable_set('store_address', $form_state['values']['store_address']);
  drupal_set_message('The store address was saved.');
  
  // Save consumer key & secret to db.
  variable_set('oauth_consumer_key',    $form_state['values']['oauth_consumer_key']);
  variable_set('oauth_consumer_secret', $form_state['values']['oauth_consumer_secret']);
  drupal_set_message('The consumer key & secret was saved.');
  
  // Default params.
  $params = _default_params();
 
  // Generate a URL for request_token.
  $url = url(STORE1 . '/oauth/request_token', array('query' => $params));
  $respone = drupal_http_request($url);
  
  // Parse the respone data to an array.
  $request_token = _parse_token_respone($respone->data);
  
  // Save the token secret to db - to be used in the access tiken request.
  variable_set('oauth_token_secret', $request_token['oauth_token_secret']);
  
//  reset($request_token);  
//  $token = array(key($request_token) => $request_token[key($request_token)]/*, 'oauth_callback' => 'http://localhost/drupal-8'*/);
//  
  // Generate a URL to redirect user to allow the application access to store. 
  $params = array('oauth_token' => $request_token['oauth_token']/*, 'oauth_callback' => 'http://localhost/drupal-8'*/);
  $url = url(STORE1 . '/oauth/authorize', array('query' => $params, 'external' => TRUE));
  drupal_goto($url);
}

/**
 * Util function: Generate current nonce.
 */
function _generate_nonce() {
  $mt = microtime();
  $rand = mt_rand();

  return md5($mt . $rand);
}

/**
 * Util function: Encode a URL string.
 */
function _urlencode_rfc3986($input){
  if (is_array($input)) {
    return array_map('_urlencode_rfc3986', $input);
  }
  elseif (is_scalar($input)) {
    return str_replace('+',' ',str_replace('%7E', '~', rawurlencode($input)));
  }
  else{
    return '';
  }
}

/**
 * Util function: Parse a token respone data.
 */
function _parse_token_respone($data) {
  if (isset($data) && $data) {
    // Parse the respone data to an array.
    $data = explode("&", $data);
    foreach ($data as $pair) {
      list ($key, $value) = explode ('=', $pair);
      $token[$key] = $value;
    }
    return $token;
  }
}