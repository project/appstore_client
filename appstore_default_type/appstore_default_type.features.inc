<?php
/**
 * @file
 * appstore_default_type.features.inc
 */

/**
 * Implements hook_default_appstore_type().
 */
function appstore_default_type_default_appstore_type() {
  $items = array();
  $items['default_store'] = entity_import('appstore_type', '{ "type" : "default_store", "label" : "Default store", "weight" : "0" }');
  return $items;
}
