<?php

/**
 * @file
 * Appstore type editing UI.
 */

/**
 * UI controller.
 */
class AppstoreTypeUIController extends EntityDefaultUIController {

  /**
   * Overrides hook_menu() defaults.
   */
  public function hook_menu() {
    $items = parent::hook_menu();
		$items[$this->path]['description'] = 'Manage appstore entity types, including adding
		and removing fields and the display of fields.';
    return $items;
  }
}

/**
 * Generates the appstore type editing form.
 * The name should match the name of the base db table. 
 */
function appstore_type_form($form, &$form_state, $appstore_type, $op = 'edit') {

  if ($op == 'clone') {
    $appstore_type->label .= ' (cloned)';
    $appstore_type->type = '';
  }

  $form['label'] = array(
    '#title' => t('Label'),
    '#type' => 'textfield',
    '#default_value' => $appstore_type->label,
    '#description' => t('The human-readable name of this appstore type.'),
    '#required' => TRUE,
    '#size' => 60,
  );
  // Machine-readable type name.
  $form['type'] = array(
    '#type' => 'machine_name',
    '#default_value' => isset($appstore_type->type) ? $appstore_type->type : '',
    '#maxlength' => 32,
//    '#disabled' => $appstore_type->isLocked() && $op != 'clone',
    '#machine_name' => array(
      'exists' => 'appstore_entity_get_types',
      'source' => array('label'),
    ),
    '#description' => t('A unique machine-readable name for this appstore type. It must only contain lowercase letters, numbers, and underscores.'),
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save appstore type'),
    '#weight' => 40,
  );

  //Locking not supported yet
  /*if (!$appstore_type->isLocked() && $op != 'add') {
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete appstore type'),
      '#weight' => 45,
      '#limit_validation_errors' => array(),
      '#submit' => array('appstore_entity_type_form_submit_delete')
    );
  }*/
  return $form;
}

/**
 * Form API submit callback for the type form.
 */
function appstore_type_form_submit(&$form, &$form_state) {
  $appstore_type = entity_ui_form_submit_build_entity($form, $form_state);
  $appstore_type->save();
  $form_state['redirect'] = 'admin/structure/appstore_types';
}

/**
 * Form API submit callback for the delete button.
 */
function appstore_type_form_submit_delete(&$form, &$form_state) {
  $form_state['redirect'] = 'admin/structure/appstore_types/manage/' . $form_state['appstore_type']->type . '/delete';
}
