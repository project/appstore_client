<?php

/**
 * @file
 * Appstore editing UI.
 *
 * We make very little use of the EntityAPI interface for this - preferring instead to use
 * views. That offers more flexibility to change a UI that will, more often than not,
 * be end-user facing.
 */

/**
 * UI controller.
 */
class AppstoreUIController extends EntityDefaultUIController {

  /**
   * Overrides hook_menu() defaults. Main reason for doing this is that
   * parent class hook_menu() is optimized for entity type administration.
   */
  public function hook_menu() {
    $items = array();
    $items = parent::hook_menu();
    $id_count = count(explode('/', $this->path));
    $wildcard = isset($this->entityInfo['admin ui']['menu wildcard']) ? $this->entityInfo['admin ui']['menu wildcard'] : '%' . $this->entityType;

//    $items[$this->path] = array(
//      'title' => 'Appstores',
//      'description' => 'Add edit and update appstores.',
//      'page callback' => 'system_admin_menu_block_page',
//      'access arguments' => array('access administration pages'),
//      'file path' => drupal_get_path('module', 'system'),
//      'file' => 'system.admin.inc',
//    );

    
    // Change the overview menu type for the list of appstores.
    $items[$this->path]['type'] = MENU_LOCAL_TASK;
    
    // Change the add page menu to multiple types of entities
    $items[$this->path . '/add'] = array(
      'title' => 'Add a appstore',
      'description' => 'Add a new appstore',
      'page callback'  => 'appstore_entity_add_page',
      'access callback'  => 'appstore_entity_access',
      'access arguments' => array('edit'),
      'type' => MENU_NORMAL_ITEM,
      'weight' => 20,
      'file' => 'appstore_entity.admin.inc',
      'file path' => drupal_get_path('module', $this->entityInfo['module'])

    );
    
    // Add menu items to add each different type of entity.
    foreach (appstore_entity_get_types() as $type) {
      $items[$this->path . '/add/' . $type->type] = array(
        'title' => 'Add ' . $type->label,
        'page callback' => 'appstore_entity_form_wrapper',
        'page arguments' => array(appstore_entity_create(array('type' => $type->type))),
        'access callback' => 'appstore_entity_access',
        'access arguments' => array('edit', 'edit ' . $type->type),
        'file' => 'appstore_entity.admin.inc',
        'file path' => drupal_get_path('module', $this->entityInfo['module'])
      );
    }

    // Loading and editing appstore entities
    $items[$this->path . '/manage/' . $wildcard] = array(
      'page callback' => 'appstore_entity_form_wrapper',
      'page arguments' => array($id_count + 1),
      'access callback' => 'appstore_entity_access',
      'access arguments' => array('edit', $id_count + 1),
      'weight' => 0,
      'context' => MENU_CONTEXT_PAGE | MENU_CONTEXT_INLINE,
      'file' => 'appstore_entity.admin.inc',
      'file path' => drupal_get_path('module', $this->entityInfo['module'])
    );
    $items[$this->path . '/manage/' . $wildcard . '/edit'] = array(
      'title' => 'Edit',
      'type' => MENU_DEFAULT_LOCAL_TASK,
      'weight' => -10,
      'context' => MENU_CONTEXT_PAGE | MENU_CONTEXT_INLINE,
    );
    
    $items[$this->path . '/manage/' . $wildcard . '/delete'] = array(
      'title' => 'Delete',
      'page callback' => 'appstore_entity_delete_form_wrapper',
      'page arguments' => array($id_count + 1),
      'access callback' => 'appstore_entity_access',
      'access arguments' => array('edit', $id_count + 1),
      'type' => MENU_LOCAL_TASK,
      'context' => MENU_CONTEXT_INLINE,
      'weight' => 10,
      'file' => 'appstore_entity.admin.inc',
      'file path' => drupal_get_path('module', $this->entityInfo['module'])
    );
    
    // Menu item for viewing appstores
    $items['appstore/' . $wildcard] = array(
      //'title' => 'Title',
      'title callback' => 'appstore_entity_page_title',
      'title arguments' => array(1),
      'page callback' => 'appstore_entity_page_view',
      'page arguments' => array(1),
      'access callback' => 'appstore_entity_access',
      'access arguments' => array('view', 1),
      'type' => MENU_CALLBACK,
    );
    return $items;
  }
  
  
  /**
   * Create the markup for the add Appstore Entities page within the class
   * so it can easily be extended/overriden.
   */ 
  public function addPage() {
    $item = menu_get_item();
    $content = system_admin_menu_block($item);

    if (count($content) == 1) {
      $item = array_shift($content);
      drupal_goto($item['href']);
    }    
        
    return theme('appstore_entity_add_list', array('content' => $content));
  }
  
}


/**
 * Form callback wrapper: create or edit a appstore.
 *
 * @param $appstore
 *   The appstore object being edited by this form.
 *
 * @see appstore_edit_form()
 */
function appstore_entity_form_wrapper($appstore) {
  if (isset($appstore) && is_numeric($appstore)) {
    $appstore = appstore_entity_load($appstore);
  }
  // Add the breadcrumb for the form's location.
  appstore_entity_set_breadcrumb();
  return drupal_get_form('appstore_entity_edit_form', $appstore);
}



/**
 * Form callback wrapper: delete a appstore.
 *
 * @param $appstore
 *   The appstore object being edited by this form.
 *
 * @see appstore_edit_form()
 */
function appstore_entity_delete_form_wrapper($appstore_id) {
  $appstore = appstore_entity_load($appstore_id);
  // Add the breadcrumb for the form's location.
  //appstore_entity_set_breadcrumb();
  return drupal_get_form('appstore_entity_delete_form', $appstore);
}


/**
 * Form callback: create or edit a appstore.
 *
 * @param $appstore
 *   The appstore object to edit or for a create form an empty appstore object
 *     with only a appstore type defined.
 */
function appstore_entity_edit_form($form, &$form_state, $appstore) { 
  // Add the default field elements.
  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Appstore Name'),
    '#default_value' => isset($appstore->name) ? $appstore->name : '',
    '#maxlength' => 255,
    '#required' => TRUE,
    '#weight' => -5,
  );
  $form['store_url'] = array(
    '#type' => 'textfield', 
    '#title' => t('Store address'), 
    '#description' => t('Please enter the store URL address Ex. http://www.example.com'),
    '#default_value' => isset($appstore->store_url) ? $appstore->store_url : '', 
    '#size' => 60, 
    '#maxlength' => 128, 
    '#required' => TRUE,
  );
  
  $form['store_key'] = array(
    '#type' => 'textfield', 
    '#title' => t('Store key'), 
    '#description' => t('Please enter your store key'),
    '#default_value' => isset($appstore->store_key) ? $appstore->store_key : '', 
    '#size' => 60, 
    '#maxlength' => 128, 
    '#required' => TRUE,
  );
  
  $form['store_secret'] = array(
    '#type' => 'textfield', 
    '#title' => t('Store secret'), 
    '#description' => t('Please enter your store secret'),
    '#default_value' => isset($appstore->store_secret) ? $appstore->store_secret : '',
    '#size' => 60, 
    '#maxlength' => 128, 
    '#required' => TRUE,
  );
 

  // Add the field related form elements.
  $form_state['appstore'] = $appstore;
  field_attach_form('appstore', $appstore, $form, $form_state);

  $form['actions'] = array(
    '#type' => 'container',
    '#attributes' => array('class' => array('form-actions')),
    '#weight' => 400,
  );

  // We add the form's #submit array to this button along with the actual submit
  // handler to preserve any submit handlers added by a form callback_wrapper.
  $submit = array();

  if (!empty($form['#submit'])) {
    $submit += $form['#submit'];
  }

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save appstore'),
    '#submit' => $submit + array('appstore_entity_edit_form_submit'),
  );
  
  if (!empty($appstore->name)) {
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete appstore'),
      '#suffix' => l(t('Cancel'), 'admin/structure/appstores'),
      '#submit' => $submit + array('appstore_entity_form_submit_delete'),
      '#weight' => 45,
    );
  }

  // We append the validate handler to #validate in case a form callback_wrapper
  // is used to add validate handlers earlier.
  $form['#validate'][] = 'appstore_entity_edit_form_validate';
  return $form;
}


/**
 * Form API validate callback for the appstore form
 */
function appstore_entity_edit_form_validate(&$form, &$form_state) {
  $appstore = $form_state['appstore'];
//  dpm(parse_url('http://'  . $form_state['store_url']));
   
  // Notify field widgets to validate their data.
  field_attach_form_validate('appstore', $appstore, $form, $form_state);
}


/**
 * Form API submit callback for the appstore form.

 */
function appstore_entity_edit_form_submit(&$form, &$form_state) {
  // Save the appstore and go back to the list of appstores
  $appstore = entity_ui_controller('appstore')->entityFormSubmitBuildEntity($form, $form_state);
 
  $store_url = rtrim($appstore->store_url, '/');
  $key = $appstore->store_key;
  $secret = $appstore->store_secret;
  
  $consumer = DrupalOAuthConsumer::load($key, FALSE);
  if (!$consumer) {
    $consumer = new DrupalOAuthConsumer($key, $secret);
    $consumer->write();
  }
  $sig_method = DrupalOAuthClient::signatureMethod();
  $client = new DrupalOAuthClient($consumer, NULL, $sig_method);

  $request_token = $client->getRequestToken($store_url . '/oauth/request_token', array(
    'callback' => url('appstore_client/access', array('absolute' => TRUE)),
  ));

  $request_token->write();

  $_SESSION['appstore_client_request_key'] = $request_token->key;

  $auth_url = $client->getAuthorizationUrl($store_url . '/oauth/authorize', array(
    'callback' => url('appstore_client/access', array('absolute' => TRUE)),
  ));
  
  $appstore->store_key = $key;
  $appstore->store_secret = $secret;
  $appstore->is_new = isset($appstore->is_new) ? $appstore->is_new : 0;
    
  $appstore->save();
  //saving the new store ID to sassion.
  $_SESSION['appstore_id'] = $appstore->appstore_id;
  
  drupal_goto($auth_url);
}

/**
 * Form API submit callback for the delete button.
 * 
 * @todo Remove hard-coded path
 */
function appstore_entity_form_submit_delete(&$form, &$form_state) {
  $form_state['redirect'] = 'admin/structure/appstores/manage/' . $form_state['appstore']->appstore_id . '/delete';
}


/**
 * Form callback: confirmation form for deleting a appstore.
 *
 * @param $appstore
 *   The appstore to delete
 *
 * @see confirm_form()
 */
function appstore_entity_delete_form($form, &$form_state, $appstore) {
  $form_state['appstore'] = $appstore;

  $form['#submit'][] = 'appstore_entity_delete_form_submit';

  $form = confirm_form($form,
    t('Are you sure you want to delete appstore %name?', array('%name' => $appstore->name)),
    'admin/structure/appstores/appstore',
    '<p>' . t('This action cannot be undone.') . '</p>',
    t('Delete'),
    t('Cancel'),
    'confirm'
  );
  
  return $form;
}

/**
 * Submit callback for appstore_delete_form
 */
function appstore_entity_delete_form_submit($form, &$form_state) {
  $appstore = $form_state['appstore'];

  appstore_entity_delete($appstore);

  drupal_set_message(t('The appstore %name has been deleted.', array('%name' => $appstore->name)));
  watchdog('appstore', 'Deleted appstore %name.', array('%name' => $appstore->name));

  $form_state['redirect'] = 'admin/structure/appstores';
}



/**
 * Page to add Appstore Entities.
 *
 * @todo Pass this through a proper theme function
 */
function appstore_entity_add_page() {
  $controller = entity_ui_controller('appstore');
  return $controller->addPage();
}


/**
 * Displays the list of available appstore types for appstore creation.
 *
 * @ingroup themeable
 */
function theme_appstore_entity_add_list($variables) {
  $content = $variables['content'];
  $output = '';
  if ($content) {
    $output = '<dl class="appstore-type-list">';
    foreach ($content as $item) {
      $output .= '<dt>' . l($item['title'], $item['href']) . '</dt>';
      $output .= '<dd>' . filter_xss_admin($item['description']) . '</dd>';
    }
    $output .= '</dl>';
  }
  else {
    if (user_access('administer appstore types')) {
      $output = '<p>' . t('Appstore Entities cannot be added because you have not created any appstore types yet. Go to the <a href="@create-appstore-type">appstore type creation page</a> to add a new appstore type.', array('@create-appstore-type' => url('admin/structure/appstore_types/add'))) . '</p>';
    }
    else {
      $output = '<p>' . t('No appstore types have been created yet for you to use.') . '</p>';
    }
  }

  return $output;
}





/**
 * Sets the breadcrumb for administrative appstore pages.
 */
function appstore_entity_set_breadcrumb() {
  $breadcrumb = array(
    l(t('Home'), '<front>'),
    l(t('Administration'), 'admin'),
    l(t('Content'), 'admin/content'),
    l(t('Appstores'), 'admin/structure/appstores'),
  );

  drupal_set_breadcrumb($breadcrumb);
}



