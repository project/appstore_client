<?php

/**
 * @file
 * Module for the Appstore Entity - a starting point to create your own Entity
 * and associated administration interface
 */  
function appstore_entity_init() {
}

/**
 *  Implementation of hook_menu().
 */
function appstore_entity_menu() {
  $items = array();
  
  $items['oauth/access'] = array(
    'page callback' => '_appstore_access',
    'access arguments' => array('access content'),
  );
  //arg 2 for appstore id arg 3 for fid
  //just for debug. remove when ready.
//  $items['oauth/getapp/%/%'] = array(
//    'page callback' => '_appstore_getapp',
//    'page arguments' => array(2, 3),
//    'access arguments' => array('access content'),
//  );
  return $items;
}

function _appstore_getapp($appstore_id, $fid) {
  $appstore = appstore_entity_load($appstore_id);
  $key = $appstore->store_key;
  $consumer = DrupalOAuthConsumer::load($key, FALSE);

  $oauth_token = $appstore->field_access_token_key['und'][0]['value'];
  $oauth_token_secret = $appstore->field_access_token_secret['und'][0]['value'];

  $access_token = new DrupalOAuthToken($oauth_token, $oauth_token_secret, $consumer, array(
    'type' => OAUTH_COMMON_TOKEN_TYPE_ACCESS,
  ));

  $sig_method = DrupalOAuthClient::signatureMethod();
  
  $auth = new HttpClientOAuth($consumer, $access_token, $sig_method, TRUE, TRUE);
  $formatter = new HttpClientBaseFormatter(HttpClientBaseFormatter::FORMAT_JSON);
  $client = new HttpClient($auth, $formatter); 

  $result = $client->post($appstore->store_url . '/appstore/api/app/info', $fid);

  return $result;
}

function _appstore_access() {
  $appstore = appstore_entity_load($_SESSION['appstore_id']);
  $base_url = $appstore->store_url . "/";
  $key = $appstore->store_key;

  $consumer = DrupalOAuthConsumer::load($key, FALSE);

  $request_token = DrupalOAuthToken::loadByKey($_GET['oauth_token'], $consumer, OAUTH_COMMON_TOKEN_TYPE_REQUEST);

  $client = new DrupalOAuthClient($consumer, $request_token);
  $verifier = isset($_GET['oauth_verifier']) ? $_GET['oauth_verifier'] : NULL;
  $access_token = $client->getAccessToken($base_url . 'oauth/access_token', array('verifier' => $verifier));
  $request_token->delete();

  $appstore->field_access_token_key['und'][0]['value'] = $access_token->key;
  $appstore->field_access_token_secret['und'][0]['value'] = $access_token->secret;
  $appstore->save();
  
  drupal_goto('appstore/'. $appstore->appstore_id);
  return 'Thank You';
}

/**
 * Implement hook_entity_info().
 *
 * We define two entities here - the actual entity that will hold our domain
 * specific information and an entity that holds information about the different
 * types of entities. See here: http://drupal.org/node/977380 for a discussion on this
 * choice.
 */
function appstore_entity_entity_info() {
  $return['appstore'] = array(
    'label' => t('Appstore'),
    // The entity class and controller class extend the classes provided by the
    // Entity API
    'entity class' => 'Appstore',
    'controller class' => 'AppstoreController',
    'base table' => 'appstore',
    'fieldable' => TRUE,
    'entity keys' => array(
        'id' => 'appstore_id',
        'bundle' => 'type',
    ),
    // Bundles are defined by the appstore types below
    'bundles' => array(),
    // Bundle keys tell the FieldAPI how to extract information from the bundle objects
    'bundle keys' => array(
      'bundle' => 'type',
    ),
    'label callback' => 'entity_class_label',
    'uri callback' => 'entity_class_uri',
    'creation callback' => 'appstore_entity_create',
    'access callback' => 'appstore_entity_access',
    'module' => 'appstore_entity',
    // The information below is used by the AppstoreUIController (which extends the EntityDefaultUIController)
    'admin ui' => array(
      'path' => 'admin/structure/appstores',
      'file' => 'appstore_entity.admin.inc',
      'controller class' => 'AppstoreUIController',
      'menu wildcard' => '%',
    ),
  );
  // The entity that holds information about the entity types	  
  $return['appstore_type'] = array(
    'label' => t('Appstore Type'),
    'entity class' => 'AppstoreType',
    'controller class' => 'AppstoreTypeController',
    'base table' => 'appstore_type',
    'fieldable' => FALSE,
    'bundle of' => 'appstore',
    'exportable' => TRUE,
    'entity keys' => array(
      'id' => 'stid',
      'name' => 'type',
      'label' => 'label',
    ),
    'access callback' => 'appstore_entity_type_access',
    'module' => 'appstore_entity',
    // Enable the entity API's admin UI.
    'admin ui' => array(
      'path' => 'admin/structure/appstore_types',
      'file' => 'appstore_entity_type.admin.inc',
      'controller class' => 'AppstoreTypeUIController',
    ),
  );
 
  return $return;
}


/**
 * Implements hook_entity_info_alter().
 *
 * We are adding the info about the appstore types via a hook to avoid a recursion
 * issue as loading the appstore types requires the entity info as well.
 *
 * @todo This needs to be improved
 */
function appstore_entity_entity_info_alter(&$entity_info) {
  foreach (appstore_entity_get_types() as $type => $info) {
    $entity_info['appstore']['bundles'][$type] = array(
      'label' => $info->label,
      'admin' => array(
        'path' => 'admin/structure/appstore_types/manage/%',
        'real path' => 'admin/structure/appstore_types/manage/' . $type,
        'bundle argument' => 4,
        'access arguments' => array('administer appstore types'),
      ),
    );
  }
}
  

/**
 * Implements hook_permission().
 */
function appstore_entity_permission() {
  // We set up permisssions to manage entity types, manage all entities and the
  // permissions for each individual entity
  $permissions = array(
    'administer appstore types' => array(
      'title' => t('Administer appstore types'),
      'description' => t('Create and delete fields for appstore types, and set their permissions.'),
    ),
    'administer appstores' => array(
      'title' => t('Administer appstores'),
      'description' => t('Edit and delete all appstores'),
    ),  
  );
  
  //Generate permissions per appstore 
  foreach (appstore_entity_get_types() as $type) {
    $type_name = check_plain($type->type);
    $permissions += array(
      "edit any $type_name appstore" => array(
        'title' => t('%type_name: Edit any appstore', array('%type_name' => $type->label)),
      ),
      "view any $type_name appstore" => array(
        'title' => t('%type_name: View any appstore', array('%type_name' => $type->label)),
      ),
    );
  }
  return $permissions;  
}


/**
 * Determines whether the given user has access to a appstore.
 *
 * @param $op
 *   The operation being performed. One of 'view', 'update', 'create', 'delete'
 *   or just 'edit' (being the same as 'create' or 'update').
 * @param $appstore
 *   Optionally a appstore or a appstore type to check access for. If nothing is
 *   given, access for all appstores is determined.
 * @param $account
 *   The user to check for. Leave it to NULL to check for the global user.
 * @return boolean
 *   Whether access is allowed or not.
 */
function appstore_entity_access($op, $appstore = NULL, $account = NULL) {
  if (user_access('administer appstores', $account)) {
    return TRUE;
  }
  if (isset($appstore) && $type_name = $appstore->type) {
    $op = ($op == 'view') ? 'view' : 'edit';
    if (user_access("$op any $type_name appstore", $account)) {
      return TRUE;
    }
  }
  return FALSE;
}


/**
 * Access callback for the entity API.
 */
function appstore_entity_type_access($op, $type = NULL, $account = NULL) {
  return user_access('administer appstore types', $account);
}


/**
 * Gets an array of all appstore types, keyed by the type name.
 *
 * @param $type_name
 *   If set, the type with the given name is returned.
 * @return AppstoreType[]
 *   Depending whether $type isset, an array of appstore types or a single one.
 */
function appstore_entity_get_types($type_name = NULL) {
  // entity_load will get the Entity controller for our appstore entity and call the load
  // function of that object - we are loading entities by name here.
  $types = entity_load_multiple_by_name('appstore_type', isset($type_name) ? array($type_name) : FALSE);
  return isset($type_name) ? reset($types) : $types;
}


/**
 * Menu argument loader; Load a appstore type by string.
 *
 * @param $type
 *   The machine-readable name of a appstore type to load.
 * @return
 *   A appstore type array or FALSE if $type does not exist.
 */
function appstore_entity_type_load($type) {
  return appstore_entity_get_types($type);
}


/**
 * Fetch a appstore object. Make sure that the wildcard you choose 
 * in the appstore entity definition fits the function name here.
 *
 * @param $appstore_id
 *   Integer specifying the appstore id.
 * @param $reset
 *   A boolean indicating that the internal cache should be reset.
 * @return
 *   A fully-loaded $appstore object or FALSE if it cannot be loaded.
 *
 * @see appstore_load_multiple()
 */
function appstore_entity_load($appstore_id, $reset = FALSE) {
  $appstores = appstore_entity_load_multiple(array($appstore_id), array(), $reset);
  return reset($appstores);
}


/**
 * Load multiple appstores based on certain conditions.
 *
 * @param $appstore_ids
 *   An array of appstore IDs.
 * @param $conditions
 *   An array of conditions to match against the {appstore} table.
 * @param $reset
 *   A boolean indicating that the internal cache should be reset.
 * @return
 *   An array of appstore objects, indexed by appstore_id.
 *
 * @see entity_load()
 * @see appstore_load()
 */
function appstore_entity_load_multiple($appstore_ids = array(), $conditions = array(), $reset = FALSE) {
  return entity_load('appstore', $appstore_ids, $conditions, $reset);
}


/**
 * Deletes a appstore.
 */
function appstore_entity_delete(Appstore $appstore) {
  $appstore->delete();
}


/**
 * Delete multiple appstores.
 *
 * @param $appstore_ids
 *   An array of appstore IDs.
 */
function appstore_entity_delete_multiple(array $appstore_ids) {
  entity_get_controller('appstore')->delete($appstore_ids);
}


/**
 * Create a appstore object.
 */
function appstore_entity_create($values = array()) {
  return entity_get_controller('appstore')->create($values);
}


/**
 * Saves a appstore to the database.
 *
 * @param $appstore
 *   The appstore object.
 */
function appstore_entity_save(Appstore $appstore) {
  return $appstore->save();
}


/**
 * Saves a appstore type to the db.
 */
function appstore_entity_type_save(AppstoreType $type) {
  $type->save();
}


/**
 * Deletes a appstore type from the db.
 */
function appstore_entity_type_delete(AppstoreType $type) {
  $type->delete();
}


/**
 * URI callback for appstores
 */
function appstore_entity_uri(Appstore $appstore){
  return array(
    'path' => 'appstore/' . $appstore->appstore_id,
  );
}


/**
 * Menu title callback for showing individual entities
 */
function appstore_entity_page_title($appstore_id){
  $appstore = appstore_entity_load($appstore_id);
  if ($appstore) {
    return $appstore->name;
  } 
  return FALSE;
}


/**
 * Sets up content to show an individual appstore
 * @todo - get rid of drupal_set_title();
 */
function appstore_entity_page_view($appstore_id, $view_mode = 'full') {
  $appstore = appstore_entity_load($appstore_id);
  $controller = entity_get_controller('appstore'); 
  if ($controller AND $appstore) {
    $content = $controller->view(array($appstore->appstore_id => $appstore));
    $page = $view_mode == 'full' ? TRUE : FALSE;
    $title = $appstore->name;
    $store_url = $appstore->store_url . '/apps';

    return theme('appstore_entity', array('appstore' => $content, 'title' => $title, 'store_url' => $store_url, 'page' => $page));
  } 
}


/**
 * Implements hook_views_api().
 */
function appstore_entity_views_api() {
  return array(
    'api' => 3,
    'path' => drupal_get_path('module', 'appstore_entity') . '/views',
  );
}


/**
 * Implement hook_theme().
 */
function appstore_entity_theme() {
  return array(
    'appstore_entity_add_list' => array(
      'variables' => array('content' => array()),
      'file' => 'appstore_entity.admin.inc',
    ),
   'appstore_entity' => array(
      'variables' => array('appstore' => NULL),
      'template' => 'appstore_entity',
    ),


  );
}  


/**
 * Implements hook_menu_local_tasks_alter().
 */
function appstore_entity_menu_local_tasks_alter(&$data, $router_item, $root_path) {
  // Add action link 'admin/structure/appstores/add' on 'admin/structure/appstores'.
  if ($root_path == 'admin/structure/appstores') {
    $item = menu_get_item('admin/structure/appstores/add');
    if ($item['access']) {
      $data['actions']['output'][] = array(
        '#theme' => 'menu_local_action',
        '#link' => $item,
      );
    }
  }
}


/**
 * The class used for appstore entities
 */
class Appstore extends Entity {
  
  public function __construct($values = array()) {
    parent::__construct($values, 'appstore');
  }

  protected function defaultLabel() {
    return $this->name;
  }

  protected function defaultUri() {
    return array('path' => 'appstore/' . $this->appstore_id);
  }
  
  
}


/**
 * The class used for appstore type entities
 */
class AppstoreType extends Entity {
  
  public $type;
  public $label;
  
  public function __construct($values = array()) {
    parent::__construct($values, 'appstore_type');
  }
  
}


/**
 * The Controller for Appstore entities
 */
class AppstoreController extends EntityAPIController {
  public function __construct($entityType) {
    parent::__construct($entityType);
  }


  /**
   * Create a appstore - we first set up the values that are specific
   * to our appstore schema but then also go through the EntityAPIController
   * function.
   * 
   * @param $type
   *   The machine-readable type of the appstore.
   *
   * @return
   *   A appstore object with all default fields initialized.
   */
  public function create(array $values = array()) {
    // Add values that are specific to our Appstore
    $values += array( 
      'appstore_id' => '',
      'is_new' => TRUE,
      'type' => '',
      'name' => '',
      'store_url' => '',
      'store_key' => '',
      'store_secret' => '',
    );
    
    $appstore = parent::create($values);
    return $appstore;
  }
  
  /**
   * Overriding the buildContent function to add entity specific fields
   */
//  public function buildContent($entity, $view_mode = 'full', $langcode = NULL, $content = array()) {
//    $content = parent::buildContent($entity, $view_mode, $langcode, $content);
//    $content['appstore_entity'] =  array(
//      '#markup' => theme('appstore_entity', array('appstore' => $entity)),
//    );
//    
//    return $content;
//  }
  
}


/**
 * The Controller for Appstore entities
 */
class AppstoreTypeController extends EntityAPIControllerExportable {
  public function __construct($entityType) {
    parent::__construct($entityType);
  }
  
   /**
   * Create a appstore type - we first set up the values that are specific
   * to our appstore type schema but then also go through the EntityAPIController
   * function.
   * 
   * @param $type
   *   The machine-readable type of the appstore.
   *
   * @return
   *   A appstore type object with all default fields initialized.
   */
  public function create(array $values = array()) {
    // Add values that are specific to our Appstore
    $values += array( 
      'stid' => '',
      'is_new' => TRUE,
      'type' => '',
      'label' => '',
      'weight' => '',
    );
    $appstore_type = parent::create($values);
    return $appstore_type;
  }

}