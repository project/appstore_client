<?php

/**
 * @file
 * A basic template for appstore entities
 *
 * Available variables:
 * - $apstore: The appstore
 * - $title: The name of the appstore
 * - $store_url: The standard URL for the remote store.
 * - $page: TRUE if this is the main view page $url points too.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. By default the following classes are available, where
 *   the parts enclosed by {} are replaced by the appropriate values:
 *   - entity-profile
 *   - appstore-{TYPE}
 *
 * Other variables:
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 *
 * @see template_preprocess()
 * @see template_preprocess_entity()
 * @see template_preprocess_appstore_entity()
 * @see template_process()
 */
?>
<div class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
  <?php if (!$page): ?>
    <h2<?php print $title_attributes; ?>>
      <?php print $title; ?>
    </h2>
  <?php endif; ?>

  <div class="content"<?php print $content_attributes; ?>>
    <?php
      print '<iframe id="store_frame" name="appstore_client" src="' . $store_url . '" frameborder="0" scrolling="auto" width="100%" height="800" marginwidth="5" marginheight="5" ></iframe>';;
    ?>
  </div>
</div>